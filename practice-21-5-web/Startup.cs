﻿using Microsoft.AspNetCore.Identity;
using practice_21_5_library.Models;

namespace practice_21_5_web
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(option => option.EnableEndpointRouting = false);
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseStaticFiles();


            app.UseMvc(
                r =>
                {
                    r.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}"
                        );
                });
        }
    }
}
