﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using practice_21_5_library.Data;
using practice_21_5_library.Models;

namespace practice_21_5_web.Controllers
{
    public class AccountController : Controller
    {
        AccountDataApi accountDataApi;

        public AccountController()
        {
            accountDataApi = new AccountDataApi();
        }

        [HttpGet]
        public IActionResult Login(string returnUrl)
        {
            return View(new UserLogin()
            {
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        public async Task<IActionResult> Login(UserLogin userLogin)
        {
            if (!string.IsNullOrEmpty(await accountDataApi.Token(userLogin)))
            {
                return Redirect(userLogin.ReturnUrl);
            }
            else
            {
                ModelState.AddModelError("", "Пользователь не найден");
                return View();
            }
        }


        [HttpGet]
        public IActionResult Register()
        {
            if (AuthUser.IsAuthenticated) return RedirectToAction("Index", "Home");
            return View(new UserRegistration());
        }

        [HttpPost]
        public async Task<ActionResult> Register(UserRegistration userRegistration)
        {
            List<string> resList = await accountDataApi.Register(userRegistration);
            if (resList.First().Equals("null"))
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                foreach (var err in resList)
                {
                    ModelState.AddModelError("", err);
                }
                return View(userRegistration);
            }
        }

        [HttpPost]
        public IActionResult Logout()
        {
            AuthUser.LogoutAuthUser();
            return RedirectToAction("Index", "Home");
        }
    }
}
