﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using practice_21_5_library.Data;
using practice_21_5_library.Models;
using System.Text.RegularExpressions;

namespace practice_21_5_web.Controllers
{
    public class HomeController : Controller
    {
        ContactDataApi contactDataApi;

        public HomeController()
        {
            contactDataApi = new ContactDataApi();
        }

        public async Task<IActionResult> Index(string err = "")
        {
            ViewBag.Contacts = await contactDataApi.GetContacts();
            ViewBag.Error = err;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(string lastName, string firstName, string surName, string phoneNumber,
           string address, string description)
        {

            if (string.IsNullOrEmpty(surName)) surName = "";
            if (string.IsNullOrEmpty(description)) description = "";
            bool isPhoneNumber = Regex.IsMatch(phoneNumber, @"^(\+[0-9]{11})$");
            if(isPhoneNumber)
            {
                Contact contact = new Contact
                {
                    LastName = lastName,
                    FirstName = firstName,
                    SurName = surName,
                    PhoneNumber = phoneNumber,
                    Address = address,
                    Description = description
                };
                await contactDataApi.AddContact(contact);
                return RedirectToAction("Index");
            }
            else
            {
                
                return RedirectToAction("Index", new { err = "Неверный номер телефона" });
            }

        }
    }
}
