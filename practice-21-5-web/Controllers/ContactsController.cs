﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using practice_21_5_library.Data;
using practice_21_5_library.Models;
using System.Text.RegularExpressions;

namespace practice_21_5_web.Controllers
{
    public class ContactsController : Controller
    {
        ContactDataApi contactDataApi;

        public ContactsController()
        {
            contactDataApi = new ContactDataApi();
        }

        public async Task<IActionResult> Index(int id, string err = "")
        {
            Contact currentContact = await contactDataApi.GetContacts(id);
            ViewBag.Error = err;
            return View(currentContact);
        }

        public async Task<IActionResult> Delete(int? id) => await ContactPage(id);

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            Contact contact = await contactDataApi.GetContacts(id);
            if (contact != null)
            {
                await contactDataApi.DeleteContact(id);
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Edit(int? id) => await ContactPage(id);

        [HttpPost]
        public async Task<IActionResult> Edit(int id, string lastName, string firstName, string surName, string phoneNumber,
            string address, string description)
        {
            Contact contact = await contactDataApi.GetContacts(id);
            if (contact != null)
            {
                bool isPhoneNumber = Regex.IsMatch(phoneNumber, @"^(\+[0-9]{11})$");
                if (isPhoneNumber)
                {
                    contact.LastName = lastName;
                    contact.FirstName = firstName;
                    contact.SurName = ValueOrDefault(surName);
                    contact.PhoneNumber = phoneNumber;
                    contact.Address = address;
                    contact.Description = ValueOrDefault(description);

                    await contactDataApi.UpdateContact(contact);
                }
                else
                {
                    return RedirectToAction("Index", new { id = id, err = "Неверный номер телефона" });
                }
            }

            return RedirectToAction("Index", new { id = id });
        }

        private async Task<IActionResult> ContactPage(int? id)
        {
            if (id != null)
            {
                Contact? contact = await contactDataApi.GetContacts(id);
                if (contact != null) return RedirectToAction("Index", new { id = id });
            }
            return NotFound();
        }

        private string ValueOrDefault(string value)
        {
            if (!string.IsNullOrEmpty(value)) return value;
            else return "";
        }
    }
}
