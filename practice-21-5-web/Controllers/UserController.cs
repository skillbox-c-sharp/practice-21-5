﻿using Microsoft.AspNetCore.Mvc;
using practice_21_5_library.Data;
using practice_21_5_library.Models;

namespace practice_21_5_web.Controllers
{
    public class UserController : Controller
    {
        UserDataApi userDataApi;

        public UserController()
        {
            userDataApi = new UserDataApi();
        }

        public async Task<IActionResult> Index()
        {
            if (AuthUser.Role == "admin")
            {
                ViewBag.Users = await GetUsersList();
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public async Task<ActionResult> Index(UserRegistration userRegistration)
        {
            List<string> resList = await userDataApi.Add(userRegistration);
            if (resList.First().Equals("Ok"))
            {
                return RedirectToAction("Index", "User");
            }
            else
            {
                foreach (var err in resList)
                {
                    ModelState.AddModelError("", err);
                }
                ViewBag.Users = await GetUsersList();
                return View();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            User user = await userDataApi.GetUser(id);
            if (user != null)
            {
                await userDataApi.DeleteUser(id);
            }
            return RedirectToAction("Index", "User");
        }

        private async Task<List<User>> GetUsersList()
        {
            IEnumerable<User> users = await userDataApi.GetUser();
            List<User> result = users.ToList();
            result.RemoveAll(d => d.UserName == AuthUser.UserName);
            return result;
        }
    }
}
