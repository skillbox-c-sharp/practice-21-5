﻿using Microsoft.AspNetCore.Mvc;

namespace practice_21_5_web.ViewComponents
{
    public class LogoutViewViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
