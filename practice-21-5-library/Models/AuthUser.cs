﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice_21_5_library.Models
{
    public class AuthUser
    {
        public static string Token { get; set; }
        public static string UserName { get; set; }
        public static bool IsAuthenticated { get; set; }
        public static string Role { get; set; }

        static AuthUser()
        {
            LogoutAuthUser();
        }

        public static void LoginAuthUser(string token, string userName, string role)
        {
            Token = token;
            UserName = userName;
            IsAuthenticated = true;
            Role = role;
        }

        public static void LogoutAuthUser()
        {
            Token = "";
            UserName = "";
            IsAuthenticated = false;
            Role = "";
        }
    }
}
