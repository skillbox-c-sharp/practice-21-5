﻿using System.ComponentModel.DataAnnotations;

namespace practice_21_5_library.Models
{
    public class UserRegistration
    {
        [Required, MaxLength(30)]
        public string LoginProp { get; set; }

        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password), Compare(nameof(Password))]
        public string ConfirmPassword { get; set; }
    }
}
