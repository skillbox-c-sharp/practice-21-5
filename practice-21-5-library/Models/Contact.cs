﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace practice_21_5_library.Models
{
    public class Contact
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? SurName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string? Description { get; set; }
    }
}
