﻿using System.ComponentModel.DataAnnotations;

namespace practice_21_5_library.Models
{
    public class UserLogin
    {
        [Required, MaxLength(30)]
        public string LoginProp { get; set; }

        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
    }
}
