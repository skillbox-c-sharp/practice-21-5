﻿using Newtonsoft.Json;
using practice_21_5_library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace practice_21_5_library.Data
{
    public class UserDataApi
    {
        HttpClient httpClient { get; set; }

        public UserDataApi()
        {
            httpClient = new HttpClient();
        }

        public async Task<IEnumerable<User>> GetUser()
        {
            string url = "https://localhost:7182/api/user";
            httpClient.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AuthUser.Token);
            var r = await httpClient.GetAsync(url);
            FunctionsForApi.CheckUnauthorized(r);
            string json = await r.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IEnumerable<User>>(json);
        }

        public async Task<User> GetUser(string? id)
        {
            string url = $"https://localhost:7182/api/user/{id}";
            httpClient.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AuthUser.Token);
            var r = await httpClient.GetAsync(url);
            FunctionsForApi.CheckUnauthorized(r);
            string json = await r.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<User>(json);
        }

        public async Task<List<string>> Add(UserRegistration userRegistration)
        {
            string url = "https://localhost:7182/api/user";
            httpClient.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AuthUser.Token);
            using var r = await httpClient.PostAsync(
                requestUri: url,
                content: new StringContent(JsonConvert.SerializeObject(userRegistration), Encoding.UTF8,
                mediaType: "application/json")
                );
            string[] errors = GetErrors(r);

            List<string> errList = new List<string>();
            foreach (string i in errors)
            {
                errList.Add(i);
            }
            FunctionsForApi.CheckUnauthorized(r);
            return errList;
        }

        public async Task DeleteUser(string id)
        {
            string url = $"https://localhost:7182/api/user/{id}/{AuthUser.UserName}";
            httpClient.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AuthUser.Token);
            using var r = await httpClient.DeleteAsync(url);
            FunctionsForApi.CheckUnauthorized(r);
        }

        private string[] GetErrors(HttpResponseMessage r)
        {
            return r.Content.ReadAsStringAsync().Result.Replace("[", "")
                            .Replace("]", "").Replace("}", "").Replace("\"", "")
                            .Split("{").Last().Split(":").Last().Split(",");
        }
    }
}
