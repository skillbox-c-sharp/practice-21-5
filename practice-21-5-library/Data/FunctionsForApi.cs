﻿using practice_21_5_library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace practice_21_5_library.Data
{
    internal static class FunctionsForApi
    {
        public static void CheckUnauthorized(HttpResponseMessage r)
        {
            if (r.StatusCode == HttpStatusCode.Unauthorized)
            {
                AuthUser.LogoutAuthUser();
            }
        }
    }
}
