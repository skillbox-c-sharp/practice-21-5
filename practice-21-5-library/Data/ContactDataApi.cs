﻿using Newtonsoft.Json;
using practice_21_5_library.Models;
using System.Net;
using System.Text;

namespace practice_21_5_library.Data
{
    public class ContactDataApi
    {
        HttpClient httpClient { get; set; }

        public ContactDataApi()
        {
            httpClient = new HttpClient();
        }

        public async Task<IEnumerable<Contact>> GetContacts()
        {
            string url = "https://localhost:7182/api/values";
            string json = await httpClient.GetStringAsync(url);
            return JsonConvert.DeserializeObject<IEnumerable<Contact>>(json);
        }

        public async Task<Contact> GetContacts(int? id)
        {
            string url = $"https://localhost:7182/api/values/{id}";
            string json = await httpClient.GetStringAsync(url);
            return JsonConvert.DeserializeObject<Contact>(json);
        }

        public async Task AddContact(Contact contact)
        {
            string url = "https://localhost:7182/api/values";
            httpClient.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AuthUser.Token);
            using var r = await httpClient.PostAsync(
                requestUri: url,
                content: new StringContent(JsonConvert.SerializeObject(contact), Encoding.UTF8,
                mediaType: "application/json")
                );
            FunctionsForApi.CheckUnauthorized(r);
        }

        public async Task DeleteContact(int id)
        {
            string url = $"https://localhost:7182/api/values/{id}";
            httpClient.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AuthUser.Token);
            using var r = await httpClient.DeleteAsync(url);
            FunctionsForApi.CheckUnauthorized(r);
        }

        public async Task UpdateContact(Contact contact)
        {
            string url = "https://localhost:7182/api/values/";
            httpClient.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AuthUser.Token);
            using var r = await httpClient.PutAsync(
                requestUri: url,
                content: new StringContent(JsonConvert.SerializeObject(contact), Encoding.UTF8,
                mediaType: "application/json")
                );
            FunctionsForApi.CheckUnauthorized(r);
        }


    }
}
