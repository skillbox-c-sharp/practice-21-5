﻿using Newtonsoft.Json;
using practice_21_5_library.Models;
using System.Text;

namespace practice_21_5_library.Data
{
    public class AccountDataApi
    {
        HttpClient httpClient { get; set; }
        const string errStatus = "401";

        public AccountDataApi()
        {
            httpClient = new HttpClient();
        }

        /*
        public async Task<UserLogin> Login(string returnUrl)
        {
            string url = $"https://localhost:7182/api/login?returnUrl={returnUrl}";
            string json = await httpClient.GetStringAsync(url);
            return JsonConvert.DeserializeObject<UserLogin>(json);
        }
        */

        public async Task<List<string>> Register(UserRegistration userRegistration)
        {
            string url = "https://localhost:7182/api/register";
            using var r = await httpClient.PostAsync(
                requestUri: url,
                content: new StringContent(JsonConvert.SerializeObject(userRegistration), Encoding.UTF8,
                mediaType: "application/json")
                );
            string[] errors = GetErrors(r);

            List<string> errList = new List<string>();
            foreach (string i in errors)
            {
                errList.Add(i);
            }
            return errList;
        }

        public async Task<string> Token(UserLogin userLogin)
        {
            string url = "https://localhost:7182/api/login";
            using var r = await httpClient.PostAsync(
                requestUri: url,
                content: new StringContent(JsonConvert.SerializeObject(userLogin), Encoding.UTF8,
                mediaType: "application/json")
                );

            string token = GetUserInfo(r)[0];
            if (token == errStatus)
            {
                AuthUser.LogoutAuthUser();
                token = "";
            }
            else
            {
                string userName = GetUserInfo(r)[1];
                string role = GetUserInfo(r)[2];
                AuthUser.LoginAuthUser(token, userName, role);
            }

            return token;
        }

        private string[] GetErrors(HttpResponseMessage r)
        {
            return r.Content.ReadAsStringAsync().Result.Replace("[", "")
                            .Replace("]", "").Replace("}", "").Replace("\"", "")
                            .Split("{").Last().Split(":").Last().Split(",");
        }

        private string[] GetUserInfo(HttpResponseMessage r)
        {
            string[] arr = new string[3];

            arr[0] = r.Content.ReadAsStringAsync().Result.Split("{").Last().
                Replace("}", "").Split(",").First().Split(":").Last().Replace("\"", "");
            if (arr[0] != errStatus) {
                arr[1] = r.Content.ReadAsStringAsync().Result.Split("{").Last()
                    .Replace("}", "").Split(",")[1].Split(":").Last().Replace("\"", "");
                arr[2] = r.Content.ReadAsStringAsync().Result.Split("{").Last().
                        Replace("}", "").Split(",").Last().Split(":").Last().Replace("\"", "");
            }

            return arr;
        }
    }
}
