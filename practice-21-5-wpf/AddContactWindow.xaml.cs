﻿using practice_21_5_wpf.Presenters;
using practice_21_5_wpf.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace practice_21_5_wpf
{
    /// <summary>
    /// Логика взаимодействия для AddContactWindow.xaml
    /// </summary>
    public partial class AddContactWindow : Window, IAddContactView
    {
        AddContactPresenter addContactPresenter;

        public AddContactWindow()
        {
            InitializeComponent();
            addContactPresenter = new AddContactPresenter(this);
        }

        private async void AddCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            bool isSuccess = await addContactPresenter.Add();
            if (isSuccess) { DialogResult = true; }
            else MessageBox.Show("Заполните все необходимые поля!");
        }

        public string LastName => LastNameTextBox.Text;

        public string FirstName => FirstNameTextBox.Text;

        public string SurName => SurNameTextBox.Text;

        public string PhoneNumber => PhoneNumberTextBox.Text;

        public string Address => AddressTextBox.Text;

        public string Description => DescriptionTextBox.Text;
    }
}
