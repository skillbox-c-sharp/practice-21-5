﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice_21_5_wpf
{
    internal class ConstantsStatus
    {
        public const string IncorrectPhoneNumberString = "incorrectPhoneNumber";
        public const string NothingChangedString = "NothingChanged";
        public const string SuccessString = "Success";

    }
}
