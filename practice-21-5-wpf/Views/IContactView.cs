﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice_21_5_wpf.Views
{
    internal interface IContactView
    {
        public string LastName { get; }
        public string FirstName { get; }
        public string SurName { get; }
        public string PhoneNumber { get; }
        public string Address { get; }
        public string Description { get; }
    }
}
