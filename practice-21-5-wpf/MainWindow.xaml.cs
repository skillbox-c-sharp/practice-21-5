﻿using practice_21_5_library.Models;
using practice_21_5_wpf.Presenters;
using practice_21_5_wpf.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace practice_21_5_wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IAllContactsView
    {
        AllContactsPresenter allContactsPresenter;
        LogoutPresenter logoutPresenter;
        Contact contactEdit;
        List<Contact> contactsList;

        public MainWindow()
        {
            InitializeComponent();
            Prepare();
        }

        private void Prepare()
        {
            allContactsPresenter = new AllContactsPresenter(this);
            logoutPresenter = new LogoutPresenter();
            contactEdit = new Contact();
            contactsList = new List<Contact>();
            LogoutState();
        }

        private async Task RefreshData()
        {
            await allContactsPresenter.GetContacts();
            dataGrid.Dispatcher.Invoke(dataGrid.Items.Refresh);
        }

        private async void MenuItemDeleteContactClick(object sender, RoutedEventArgs e)
        {
            Contact contact = (Contact)dataGrid.SelectedItem;
            int id = contact.Id;
            await allContactsPresenter.Delete(id);
            await RefreshData();
        }

        private async void MenuItemAddContactClick(object sender, RoutedEventArgs e)
        {
            AddContactWindow addContactWindow = new AddContactWindow();
            addContactWindow.ShowDialog();
            if (addContactWindow.DialogResult.Value)
            {
                await RefreshData();
            }
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            await RefreshData();
        }

        private void openDialogLoginButton_Click(object sender, RoutedEventArgs e)
        {
            if (!AuthUser.IsAuthenticated)
            {
                LoginAction();
            }
            else
            {
                LogoutAction();
            }
        }

        private async void dataGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            if (AuthUser.Role.Equals("admin"))
            {
                if (dataGrid.SelectedIndex == -1) return;
                dataGrid.CommitEdit();
                SetContactEdit();
                string edit = await allContactsPresenter.Edit();
                if (edit.Equals(ConstantsStatus.IncorrectPhoneNumberString)) MessageBox.Show("Неверный номер телефона");
                await RefreshData();
            }
            else return;
            
        }

        private void dataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            dataGrid.BeginEdit();
        }

        private void LoginAction()
        {
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.ShowDialog();
            if (loginWindow.DialogResult.Value)
            {
                if (AuthUser.Role.Equals("admin"))
                {
                    LoginAdminState();
                }
                else
                {
                    LoginUserState();
                }
            }
        }

        private void LogoutAction()
        {
            logoutPresenter.Logout();
            LogoutState();
        }

        private void LogoutState()
        {
            StatusLabel.Content = "Войдите в систему";
            openDialogLoginButton.Content = "Войти";
            dataGrid.IsReadOnly = true;
            DeleteContactContextButton.Visibility = Visibility.Collapsed;
            AddContactContextButton.Visibility = Visibility.Collapsed;
        }
        private void LoginAdminState()
        {
            StatusLabel.Content = AuthUser.UserName;
            openDialogLoginButton.Content = "Выйти";
            dataGrid.IsReadOnly = false;
            DeleteContactContextButton.Visibility = Visibility.Visible;
            AddContactContextButton.Visibility = Visibility.Visible;
        }

        private void LoginUserState()
        {
            StatusLabel.Content = AuthUser.UserName;
            openDialogLoginButton.Content = "Выйти";
            dataGrid.IsReadOnly = true;
            DeleteContactContextButton.Visibility = Visibility.Collapsed;
            AddContactContextButton.Visibility = Visibility.Visible;
        }

        private void SetContactEdit()
        {
            contactEdit.Id = ((Contact)dataGrid.SelectedItem).Id;
            contactEdit.LastName = ((Contact)dataGrid.SelectedItem).LastName;
            contactEdit.FirstName = ((Contact)dataGrid.SelectedItem).FirstName;
            contactEdit.SurName = ((Contact)dataGrid.SelectedItem).SurName;
            contactEdit.PhoneNumber = ((Contact)dataGrid.SelectedItem).PhoneNumber;
            contactEdit.Address = ((Contact)dataGrid.SelectedItem).Address;
            contactEdit.Description = ((Contact)dataGrid.SelectedItem).Description;
        }

        public List<Contact> AllContacts { 
            set
            { 
                contactsList = value;
                dataGrid.ItemsSource = contactsList;
                dataGrid.Items.Refresh();
            } 
        }
        public int Id => contactEdit.Id;

        public string LastName => contactEdit.LastName;

        public string FirstName => contactEdit.FirstName;

        public string SurName => contactEdit.SurName;

        public string PhoneNumber => contactEdit.PhoneNumber;

        public string Address => contactEdit.Address;

        public string Description => contactEdit.Description;
    }
}
