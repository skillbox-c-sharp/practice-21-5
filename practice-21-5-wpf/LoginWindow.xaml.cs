﻿using practice_21_5_wpf.Presenters;
using practice_21_5_wpf.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace practice_21_5_wpf
{
    /// <summary>
    /// Логика взаимодействия для LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window, IAccountView
    {
        AccountPresenter accountPresenter;

        public LoginWindow()
        {
            InitializeComponent();
            cancelButton.Click += delegate { DialogResult = false; };
            accountPresenter = new AccountPresenter(this);
        }

        private async void loginButton_Click(object sender, RoutedEventArgs e)
        {
            string login = loginTextBox.Text;
            string password = passwordTextBox.Password.ToString();
            bool isLoggined = false;

            if (!(string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password)))
            {
                isLoggined = await accountPresenter.Login(login, password);
            }
            else
            {
                MessageBox.Show("Введите логин и пароль для входа");
            }
            if (isLoggined) DialogResult = true;
        }

        public string Status { set => LoginStatusLabel.Content = value; }
    }
}
