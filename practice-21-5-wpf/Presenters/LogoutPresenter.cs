﻿using practice_21_5_library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice_21_5_wpf.Presenters
{
    internal class LogoutPresenter
    {
        public void Logout()
        {
            AuthUser.LogoutAuthUser();
        }
    }
}
