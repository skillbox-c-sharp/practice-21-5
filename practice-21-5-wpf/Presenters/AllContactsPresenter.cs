﻿using practice_21_5_library.Data;
using practice_21_5_library.Models;
using practice_21_5_wpf.Views;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;

namespace practice_21_5_wpf.Presenters
{
    internal class AllContactsPresenter
    {
        IAllContactsView allContactsView;
        ContactDataApi contactDataApi;

        public AllContactsPresenter(IAllContactsView view)
        {
            contactDataApi = new ContactDataApi();
            allContactsView = view;
        }

        public async Task GetContacts()
        {
            await GetContactsList();
        }

        public async Task Delete(int id)
        {
            Contact contact = await contactDataApi.GetContacts(id);
            if (contact != null)
            {
                await contactDataApi.DeleteContact(id);
            }
            await GetContactsList();
        }

        public async Task<string> Edit()
        {
            Contact contact = await contactDataApi.GetContacts(allContactsView.Id);
            if (contact != null)
            {
                if (IsChanged(contact))
                {
                    if (!string.IsNullOrEmpty(allContactsView.FirstName))
                        contact.FirstName = allContactsView.FirstName;

                    if (!string.IsNullOrEmpty(allContactsView.LastName))
                        contact.LastName = allContactsView.LastName;

                    contact.SurName = ValueOrDefault(allContactsView.SurName);

                    bool isPhoneNumber = Regex.IsMatch(allContactsView.PhoneNumber, @"^(\+[0-9]{11})$");
                    if (isPhoneNumber)
                        contact.PhoneNumber = allContactsView.PhoneNumber;
                    else
                        return ConstantsStatus.IncorrectPhoneNumberString;

                    if (!string.IsNullOrEmpty(allContactsView.Address))
                        contact.Address = allContactsView.Address;

                    contact.Description = ValueOrDefault(allContactsView.Description);

                    await contactDataApi.UpdateContact(contact);
                }
                else
                {
                    return ConstantsStatus.NothingChangedString;
                }
            }
            await GetContactsList();
            return ConstantsStatus.SuccessString;
        }

        private async Task GetContactsList()
        {
            IEnumerable<Contact> contacts = await contactDataApi.GetContacts();
            allContactsView.AllContacts = contacts.ToList();
        }

        private string ValueOrDefault(string value)
        {
            if (!string.IsNullOrEmpty(value)) return value;
            else return "";
        }

        private bool IsChanged(Contact contact)
        {
            bool isChanged = false;
            if (!contact.FirstName.Equals(allContactsView.FirstName)
                || !contact.LastName.Equals(allContactsView.LastName)
                || !contact.SurName.Equals(allContactsView.SurName)
                || !contact.PhoneNumber.Equals(allContactsView.PhoneNumber)
                || !contact.Address.Equals(allContactsView.Address)
                || !contact.Description.Equals(allContactsView.Description))
            {
                isChanged = true;
            }
            return isChanged;
        }
    }
}
