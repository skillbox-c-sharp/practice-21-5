﻿using practice_21_5_library.Data;
using practice_21_5_library.Models;
using practice_21_5_wpf.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice_21_5_wpf.Presenters
{
    internal class AddContactPresenter
    {
        IAddContactView accountView;
        ContactDataApi contactDataApi;

        public AddContactPresenter(IAddContactView view)
        {
            accountView = view;
            contactDataApi = new ContactDataApi();
        }

        public async Task<bool> Add()
        {
            if (CheckFields())
            {
                string surName = accountView.SurName;
                string description = accountView.Description;
                if (string.IsNullOrEmpty(surName)) surName = "";
                if (string.IsNullOrEmpty(description)) description = "";
                Contact contact = new Contact
                {
                    LastName = accountView.LastName,
                    FirstName = accountView.FirstName,
                    SurName = surName,
                    PhoneNumber = accountView.PhoneNumber,
                    Address = accountView.Address,
                    Description = description
                };
                await contactDataApi.AddContact(contact);
                return true;
            }
            else return false;
        }

        private bool CheckFields()
        {
            if(string.IsNullOrEmpty(accountView.LastName)) return false;
            if(string.IsNullOrEmpty(accountView.FirstName)) return false;
            if(string.IsNullOrEmpty(accountView.PhoneNumber)) return false;
            if(string.IsNullOrEmpty(accountView.Address)) return false;
            return true;
        }
    }
}
