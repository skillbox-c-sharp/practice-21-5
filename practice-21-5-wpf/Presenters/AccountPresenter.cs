﻿using Microsoft.EntityFrameworkCore.Metadata;
using practice_21_5_library.Data;
using practice_21_5_library.Models;
using practice_21_5_wpf.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice_21_5_wpf.Presenters
{
    internal class AccountPresenter
    {
        IAccountView accountView;
        AccountDataApi accountDataApi;

        public AccountPresenter(IAccountView view)
        {
            accountView = view;
            accountDataApi = new AccountDataApi();
        }

        public async Task<bool> Login(string login, string password)
        {
            UserLogin userLogin = new UserLogin()
            {
                LoginProp = login,
                Password = password,
                ReturnUrl = "/"
            };
            if (!string.IsNullOrEmpty(await accountDataApi.Token(userLogin)))
            {
                return true;
            }
            else
            {
                string err = "Пользователь не найден";
                accountView.Status = err;
                return false;
            }
        }
    }
}
