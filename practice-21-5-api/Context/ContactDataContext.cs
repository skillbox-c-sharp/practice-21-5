﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using practice_21_5_library.Models;

namespace practice_21_5_api.Context
{
    public class ContactDataContext : IdentityDbContext<User>
    {
        public DbSet<Contact> Contacts { get; set; }
        public ContactDataContext() => Database.EnsureCreated();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                "Server=(localdb)\\MSSQLLocalDB;Database=ContactsDB;Trusted_Connection=True;"
            );
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Contact>().HasData(
                new Contact
                {
                    Id = 1,
                    FirstName = "Ivan",
                    LastName = "Parshin",
                    SurName = "Sergeevich",
                    PhoneNumber = "+79069096528",
                    Address = "Санкт-Петербург, Ленина, 20",
                    Description = "Телефон устарел"
                },
                new Contact
                {
                    Id = 2,
                    FirstName = "Semen",
                    LastName = "Semenov",
                    SurName = "Semenovich",
                    PhoneNumber = "+79112544575",
                    Address = "Новосибирск, Столетова, 65",
                    Description = "Не отвечает на звонки"
                },
                new Contact
                {
                    Id = 3,
                    FirstName = "Ekaterina",
                    LastName = "Firsanova",
                    SurName = "Yurievna",
                    PhoneNumber = "+79845548785",
                    Address = "Москва, Сретенка, 54",
                    Description = "Звонить после 15:00"
                }
            );

            const string ADMIN_ID = "a18be9c0-aa65-4af8-bd17-00bd9344e575";
            const string ROLE_ID = "ad376a8f-9eab-4bb9-9fca-30b01540f445";

            modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = ROLE_ID,
                Name = "admin",
                NormalizedName = "admin"
            });

            var admin = new User()
            {
                Id = ADMIN_ID,
                UserName = "admin",
                NormalizedUserName = "admin",
            };

            admin.PasswordHash = PassGenerate(admin);

            modelBuilder.Entity<User>().HasData(admin);

            modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = ROLE_ID,
                UserId = ADMIN_ID
            });
        }

        public string PassGenerate(User user)
        {
            var passHash = new PasswordHasher<User>();
            return passHash.HashPassword(user, "password");
        }
    }
}
