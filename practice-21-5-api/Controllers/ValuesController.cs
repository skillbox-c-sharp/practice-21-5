﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using practice_21_5_api.Context;
using practice_21_5_library.Models;

namespace practice_21_5_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        readonly ContactDataContext contactsDb;

        public ValuesController(ContactDataContext context)
        {
            contactsDb = context;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Contact>>> Get()
        {
            return await contactsDb.Contacts.ToListAsync();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Contact>> Get(int id)
        {
            Contact contact = await contactsDb.Contacts.FirstOrDefaultAsync(x => x.Id == id);
            if (contact == null)
                return NotFound();
            return new ObjectResult(contact);
        }

        // POST api/values
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<Contact>> Post([FromBody] Contact contact)
        {
            if (contact == null)
            {
                return BadRequest();
            }

            contactsDb.Contacts.Add(contact);
            await contactsDb.SaveChangesAsync();
            return Ok(contact);
        }

        // PUT api/values/
        [HttpPut]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public async Task<ActionResult<Contact>> Put([FromBody] Contact contact)
        {
            if (contact == null)
            {
                return BadRequest();
            }
            if (!contactsDb.Contacts.Any(x => x.Id == contact.Id))
            {
                return NotFound();
            }

            contactsDb.Update(contact);
            await contactsDb.SaveChangesAsync();
            return Ok(contact);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public async Task<ActionResult<Contact>> Delete(int id)
        {
            Contact contact = contactsDb.Contacts.FirstOrDefault(x => x.Id == id);
            if (contact == null)
            {
                return NotFound();
            }
            contactsDb.Contacts.Remove(contact);
            await contactsDb.SaveChangesAsync();
            return Ok(contact);
        }
    }
}
