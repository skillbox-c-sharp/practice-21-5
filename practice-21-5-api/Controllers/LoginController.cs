﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using practice_21_5_api.Context;
using practice_21_5_library.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace practice_21_5_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoginController : ControllerBase
    {
        readonly SignInManager<User> _signInManager;
        readonly ContactDataContext _contactDataContext;

        public LoginController(SignInManager<User> signInManager)
        {
            _signInManager = signInManager;
            _contactDataContext = new ContactDataContext();
        }

        /*
        [HttpGet]
        public UserLogin Login(string returnUrl)
        {
            return new UserLogin()
            {
                ReturnUrl = returnUrl
            };
        }
        */

        [HttpPost]
        public async Task<IResult> Login(UserLogin userLogin)
        {
            if (ModelState.IsValid)
            {
                var loginResult = await _signInManager.PasswordSignInAsync(userLogin.LoginProp,
                    userLogin.Password,
                    false,
                    lockoutOnFailure: false);

                if (loginResult.Succeeded)
                {
                    string roleString = GetRole(userLogin.LoginProp);
                    var claims = new List<Claim> { 
                        new Claim("login", userLogin.LoginProp),
                        new Claim(ClaimTypes.Role, roleString)
                    };
                    var jwt = new JwtSecurityToken(
                            issuer: AuthOptions.ISSUER,
                            audience: AuthOptions.AUDIENCE,
                            claims: claims,
                            expires: DateTime.UtcNow.Add(TimeSpan.FromMinutes(120)),
                            signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

                    var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                    var response = new
                    {
                        access_token = encodedJwt,
                        username = userLogin.LoginProp,
                        role = roleString
                    };

                    return Results.Json(response);
                }
            }
            return Results.Unauthorized();
        }

        private string GetRole(string loginProp)
        {
            string id = _contactDataContext.Users.FirstOrDefault(d => d.UserName == loginProp).Id;
            IdentityUserRole<string> identityUserRole = _contactDataContext.UserRoles.FirstOrDefault(d => d.UserId == id);
            if (identityUserRole != null)
            {
                string roleId = identityUserRole.RoleId;
                IdentityRole identityRole = _contactDataContext.Roles.FirstOrDefault(d => d.Id == roleId);
                if (identityRole != null)
                {
                    return identityRole.Name;
                }
            }
            return "";
        }
    }
}
