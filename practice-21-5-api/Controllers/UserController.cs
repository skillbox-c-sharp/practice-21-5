﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using practice_21_5_api.Context;
using practice_21_5_library.Models;

namespace practice_21_5_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        readonly ContactDataContext contactsDb;
        readonly UserManager<User> _userManager;

        public UserController(ContactDataContext context, UserManager<User> userManager)
        {
            contactsDb = context;
            _userManager = userManager;
        }

        // GET api/user
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public async Task<ActionResult<IEnumerable<User>>> Get()
        {
            return await contactsDb.Users.ToListAsync();
        }

        // GET api/user/id
        [HttpGet("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public async Task<ActionResult<User>> Get(string id)
        {
            User user = await contactsDb.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (user == null)
                return NotFound();
            return new ObjectResult(user);
        }

        // POST api/user
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public async Task<IResult> Post(UserRegistration userRegistration)
        {
            if (ModelState.IsValid)
            {
                var user = new User { UserName = userRegistration.LoginProp };
                var createResult = await _userManager.CreateAsync(user, userRegistration.Password);

                if (createResult.Succeeded)
                {
                    var response = new
                    {
                        ok = "Ok"
                    };
                    return Results.Json(response);
                }
                else
                {
                    List<string> errorsList = new List<string>();
                    foreach (var err in createResult.Errors)
                    {
                        errorsList.Add(err.Description);
                    }
                    var response = new
                    {
                        errors = errorsList
                    };
                    return Results.Json(response);
                }
            }
            return Results.Unauthorized();
        }

        // DELETE api/user/id/curUserName
        [HttpDelete("{id}/{curUserName}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public async Task<ActionResult<User>> Delete(string id, string curUserName)
        {
            User user = contactsDb.Users.FirstOrDefault(x => x.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            else
            {
                if (user.UserName != curUserName)
                {
                    contactsDb.Users.Remove(user);
                    await contactsDb.SaveChangesAsync();
                    return Ok(user);
                }
                else
                {
                    return BadRequest();
                }
            }

        }
    }
}
