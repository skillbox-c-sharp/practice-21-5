﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using practice_21_5_library.Models;

namespace practice_21_5_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RegisterController : ControllerBase
    {
        readonly UserManager<User> _userManager;

        public RegisterController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        [HttpPost]
        public async Task<IResult> Register(UserRegistration userRegistration)
        {
            if (ModelState.IsValid)
            {
                var user = new User { UserName = userRegistration.LoginProp };
                var createResult = await _userManager.CreateAsync(user, userRegistration.Password);

                if (createResult.Succeeded)
                {
                    var response = new
                    {
                        ok = "Ok"
                    };
                    return Results.Json(response);
                }
                else
                {
                    List<string> errorsList = new List<string>();
                    foreach (var err in createResult.Errors)
                    {
                        errorsList.Add(err.Description);
                    }
                    var response = new
                    {
                        errors = errorsList
                    };
                    return Results.Json(response);
                }
            }
            return Results.Unauthorized();
        }
    }
}
